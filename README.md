## Semana 26 - Integrando Fullcalendar

1. Intro a Fullcalendar
2. Integrando Fullcalendar
3. Agregando eventos
4. Guardando eventos en la base de datos
5. Mostrando eventos en el calendario
6. Eventos con campos distintos
7. Eventos por Ajax
8. Reagendando eventos
9. Capturando el día a reagendar
10. Actualizando el evento por Ajax
11. Registrando el click en el evento
12. QUIZ
13. Click Modal
14. Render modal form
15. Actualizando por Ajax
16. Integrando alertas flash
17. Variables flash y Ajax
18. Detalles finales de la alerta flash
19. Resolviendo la transformación idempotente
20. Intro a Date Time Picker
21. Integrando Date Time Picker
22. Datetimepicker
23. Iconos y posición del Widget
24. QUIZ


* Fullcalendar: 

https://yarnpkg.com/en/package/fullcalendar

https://fullcalendar.io/docs

* Momment js: https://momentjs.com/

* Datetimepicker: https://yarnpkg.com/en/package/bootstrap4-datetimepicker
